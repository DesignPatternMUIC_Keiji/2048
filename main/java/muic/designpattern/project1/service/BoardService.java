package muic.designpattern.project1.service;

import muic.designpattern.project1.entity.Board;

/**
 * Created by Trung on 9/12/2017.
 */
public interface BoardService {

    /**
     * Set board variables
     * @param board
     * @param initSpawn
     */
    void setBoard(Board board, int initSpawn);

    /**
     * Spawn content into board
     * @param board
     */
    void spawnBoardContent(Board board, int spawnCap);

    /**
     * Do move right
     * @param board
     */
    void moveRight(Board board);

    /**
     * Do move left
     * @param board
     */
    void moveLeft(Board board);

    /**
     * Do move up
     * @param board
     */
    void moveUp(Board board);

    /**
     * Do move down
     * @param board
     */
    void moveDown(Board board);

    /**
     * Check for game over status
     * @param board
     * @return
     */
    boolean checkGameOverStatus(Board board);
}
