package muic.designpattern.project1.service.impl;

import muic.designpattern.project1.entity.Board;
import muic.designpattern.project1.service.CalculationService;

import java.util.*;

/**
 * Created by Trung on 9/12/2017.
 */
public class CalculationServiceImpl implements CalculationService{

    /**
     * Calculate content changes by row for right move
     * @param board
     * @return
     */
    public Board calculateBoardContentToRight(Board board) {
        int boardRow = board.getNoOfRow();
        int boardColumn = board.getNoOfColumn();

        for (int i = boardRow-1; i >= 0; i--) {
            int currentContent = 0;
            int currentContentIndex = 0;
            for (int j = boardColumn-1; j >= 0; j--) {
                int[] contentAndIndex = this.calculateBoardContentByCondition(board, currentContent,
                        currentContentIndex, i, j, true);
                currentContent = contentAndIndex[0];
                currentContentIndex = contentAndIndex[1];
            }
        }
        return board;
    }

    /**
     * Calculate content changes by row for left move
     * @param board
     * @return
     */
    public Board calculateBoardContentToLeft(Board board) {
        int boardRow = board.getNoOfRow();
        int boardColumn = board.getNoOfColumn();

        for (int i = 0; i < boardRow; i++) {
            int currentContent = 0;
            int currentContentIndex = 0;
            for (int j = 0; j < boardColumn; j++) {
                int[] contentAndIndex = this.calculateBoardContentByCondition(board, currentContent,
                        currentContentIndex, i, j, true);
                currentContent = contentAndIndex[0];
                currentContentIndex = contentAndIndex[1];
            }
        }
        return board;
    }

    /**
     * Calculate content changes by row for up move
     * @param board
     * @return
     */
    public Board calculateBoardContentToTop(Board board) {
        int boardRow = board.getNoOfRow();
        int boardColumn = board.getNoOfColumn();

        for (int i = 0; i < boardColumn; i++) {
            int currentContent = 0;
            int currentContentIndex = 0;
            for (int j = 0; j < boardRow; j++) {
                int[] contentAndIndex = this.calculateBoardContentByCondition(board, currentContent,
                        currentContentIndex, j, i, false);
                currentContent = contentAndIndex[0];
                currentContentIndex = contentAndIndex[1];
            }
        }
        return board;
    }

    /**
     * Calculate content changes by row for down move
     * @param board
     * @return
     */
    public Board calculateBoardContentToBottom(Board board) {
        int boardRow = board.getNoOfRow();
        int boardColumn = board.getNoOfColumn();

        for (int i = boardColumn-1; i >= 0; i--) {
            int currentContent = 0;
            int currentContentIndex = 0;
            for (int j = boardRow-1; j >= 0; j--) {
                int[] contentAndIndex = this.calculateBoardContentByCondition(board, currentContent,
                        currentContentIndex, j, i, false);
                currentContent = contentAndIndex[0];
                currentContentIndex = contentAndIndex[1];
            }
        }
        return board;
    }

    /**
     * Find all blank nodes given board
     * @param board
     * @return
     */
    public List<int[]> findAllBlankNodes(Board board) {
        int boardRow = board.getNoOfRow();
        int boardColumn = board.getNoOfColumn();
        int[][] boardStructure = board.getBoardStructure();
        List<int[]> result = new ArrayList<>();

        for (int i = 0; i < boardRow; i++) {
            for (int j = 0; j < boardColumn; j++) {
                if (boardStructure[i][j] == 0)
                    result.add(new int[]{i,j});
            }
        }
        return result;
    }

    /**
     * Helper function for calculateBoardContentToLeft and calculateBoardContentToRight
     * Do not question my algorithm
     * @param board
     * @param currentContent
     * @param currentContentIndex
     * @param i
     * @param j
     * @return
     */
    private int[] calculateBoardContentByCondition(Board board, int currentContent, int currentContentIndex,
                                                   int i, int j, boolean isHorizontalCalculation){
        int[][] boardStructure = board.getBoardStructure();

        if (boardStructure[i][j] != 0) {
            if (currentContent == 0 || currentContent != boardStructure[i][j]) {
                currentContent = boardStructure[i][j];
                currentContentIndex = isHorizontalCalculation ? j : i;
            } else if (currentContent == boardStructure[i][j]) {
                currentContent += boardStructure[i][j];
                if (isHorizontalCalculation)
                    boardStructure[i][currentContentIndex] = currentContent;
                else
                    boardStructure[currentContentIndex][j] = currentContent;
                boardStructure[i][j] = 0;
            }
        }
        return new int[]{currentContent, currentContentIndex};
    }

    /**
     * x/columns=width, y/rows=height
     * require, how many grid in the board
     * We need more offset for some stuff later but right now we keep it at 0
     * @return array int of width and height respectively [width,height]
     */
    public int[] calculateMainFrameDimensions(int rows, int columns){
        int columnSize = 60;
        int rowSize = 60;
        int columnOffset = 0;
        int rowOffset = calculateMenuBarDimensions(columns)[1];
        int[] dimensions = new int[2];
        dimensions[0] = columns * columnSize + columnOffset;
        dimensions[1] = rows * rowSize + rowOffset;
        return dimensions;
    }

    public int[] calculateGamePanelDimensions(int rows, int columns){
        int columnSize = 60;
        int rowSize = 60;
        int[] dimensions = new int[2];
        dimensions[0] = columns * columnSize;
        dimensions[1] = rows * rowSize;
        return dimensions;
    }

    public int[] calculateMenuBarDimensions(int columns) {
        int[] dimensions = new int[2];
        int columnSize = 60;
        dimensions[0] = columns * columnSize;
        dimensions[1] = 40;
        return dimensions;
    }
}
