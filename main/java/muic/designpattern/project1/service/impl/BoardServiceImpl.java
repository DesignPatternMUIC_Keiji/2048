package muic.designpattern.project1.service.impl;

import muic.designpattern.project1.entity.Board;
import muic.designpattern.project1.gui.input.handler.InputHandler;
import muic.designpattern.project1.service.BoardService;
import muic.designpattern.project1.service.CalculationService;

import java.awt.event.KeyEvent;
import java.util.*;

/**
 * Created by Trung on 9/12/2017.
 */
public class BoardServiceImpl implements BoardService{

    private final CalculationService calculationService = new CalculationServiceImpl();

    /**
     * Set board variables
     * @param board
     */
    public void setBoard(Board board, int initSpawn) {
        board.setGameOver(false);
        this.fillBoard(board);
        this.spawnBoardContent(board, initSpawn);
    }

    /**
     * Spawn content into board
     * @param board
     * @param spawnCap
     */
    public void spawnBoardContent(Board board, int spawnCap) {
        int[][] boardStructure = board.getBoardStructure();
        int[] spawnableContents = {2,4};
        List<int[]> spawnableLocations = calculationService.findAllBlankNodes(board);

        while (spawnCap > 0 && spawnableLocations.size() > 0) {
            Random rand = new Random();
            // Shuffle spawn location
            Collections.shuffle(spawnableLocations);
            int randomRow = spawnableLocations.get(0)[0];
            int randomColumn = spawnableLocations.get(0)[1];

            boardStructure[randomRow][randomColumn] = rand.nextBoolean() ? spawnableContents[0] : spawnableContents[1];
            spawnableLocations.remove(0);
            spawnCap--;
        }
    }

    /**
     * Do move right
     * @param board
     */
    public void moveRight(Board board) {
        // Calculate move
        calculationService.calculateBoardContentToRight(board);

        List<List<Integer>> nonBlankMatrix = this.getNonBlankContentHorizontal(board);

        // Fill board with 0's
        this.fillBoard(board);
        this.moveBoardByCondition(board, nonBlankMatrix, true, true);
    }

    /**
     * Do move left
     * @param board
     */
    public void moveLeft(Board board) {
        // Calculate move
        calculationService.calculateBoardContentToLeft(board);

        List<List<Integer>> nonBlankMatrix = this.getNonBlankContentHorizontal(board);

        // Fill board with 0's
        this.fillBoard(board);
        this.moveBoardByCondition(board, nonBlankMatrix, true, false);
    }

    /**
     * Do move up
     * @param board
     */
    public void moveUp(Board board) {
        // Calculate move
        calculationService.calculateBoardContentToTop(board);

        List<List<Integer>> nonBlankMatrix = this.getNonBlankContentVertical(board);

        // Fill board with 0's
        this.fillBoard(board);
        this.moveBoardByCondition(board, nonBlankMatrix, false, false);
    }

    /**
     * Do move down
     * @param board
     */
    public void moveDown(Board board) {
        // Calculate move
        calculationService.calculateBoardContentToBottom(board);

        List<List<Integer>> nonBlankMatrix = this.getNonBlankContentVertical(board);

        // Fill board with 0's
        this.fillBoard(board);
        this.moveBoardByCondition(board, nonBlankMatrix, false, true);
    }

    /**
     * Check for game over status
     * @param board
     * @return
     */
    public boolean checkGameOverStatus(Board board) {
        final int[] LEGAL_MOVES = {KeyEvent.VK_UP, KeyEvent.VK_DOWN, KeyEvent.VK_LEFT, KeyEvent.VK_RIGHT};
        final InputHandler throwawayInputHandler = new InputHandler();
        int[][] originalBoard = board.getBoardStructure();

        for (int move: LEGAL_MOVES) {
            Board cloneBoard = this.clone(board);
            throwawayInputHandler.setBoard(cloneBoard);
            throwawayInputHandler.handle(move);

            int[][] postMove = cloneBoard.getBoardStructure();
            // Check if moveable
            if (!Arrays.deepEquals(postMove, originalBoard))
                return false;
        }
        return true;
    }

    /**
     * Fill board with 0's
     * @param board
     */
    private void fillBoard(Board board) {
        for (int[] subBoard: board.getBoardStructure()) {
            Arrays.fill(subBoard, 0);
        }
    }

    /**
     * Clean board of blank nodes horizontally
     * @param board
     * @return
     */
    @SuppressWarnings("Duplicates")
    private List<List<Integer>> getNonBlankContentHorizontal(Board board) {
        int boardRow = board.getNoOfRow();
        int boardColumn = board.getNoOfColumn();
        int[][] boardStructure = board.getBoardStructure();
        List<List<Integer>> nonBlankMatrix = new ArrayList<>();

        for (int i = 0; i < boardRow; i++) {
            List<Integer> nonBlankList = new ArrayList<>();
            for (int j = 0; j < boardColumn; j++) {
                if (boardStructure[i][j] != 0)
                    nonBlankList.add(boardStructure[i][j]);
            }
            nonBlankMatrix.add(nonBlankList);
        }
        return nonBlankMatrix;
    }

    /**
     * Clean board of blank nodes vertically
     * @param board
     * @return
     */
    @SuppressWarnings("Duplicates")
    private List<List<Integer>> getNonBlankContentVertical(Board board) {
        int boardRow = board.getNoOfRow();
        int boardColumn = board.getNoOfColumn();
        int[][] boardStructure = board.getBoardStructure();
        List<List<Integer>> nonBlankMatrix = new ArrayList<>();

        for (int i = 0; i < boardColumn; i++) {
            List<Integer> nonBlankList = new ArrayList<>();
            for (int j = 0; j < boardRow; j++) {
                if (boardStructure[j][i] != 0)
                    nonBlankList.add(boardStructure[j][i]);
            }
            nonBlankMatrix.add(nonBlankList);
        }
        return nonBlankMatrix;
    }

    /**
     * Move board by direction
     * Do not question the greatness of this code
     * @param board
     * @param nonBlankMatrix
     * @param isHorizontalCalculation
     * @param isReversed
     */
    @SuppressWarnings("Duplicates")
    private void moveBoardByCondition(Board board, List<List<Integer>> nonBlankMatrix,
                                      boolean isHorizontalCalculation, boolean isReversed) {
        int boardRow = board.getNoOfRow();
        int boardColumn = board.getNoOfColumn();
        int[][] boardStructure = board.getBoardStructure();

        // isHorizontalCalculation = move to right/left
        if (isHorizontalCalculation) {
            int i = 0;
            for (List<Integer> nonBlankList : nonBlankMatrix) {
                if (isReversed)
                    Collections.reverse(nonBlankList);
                int j = isReversed ? boardColumn-1 : 0;

                for (Integer content : nonBlankList) {
                    boardStructure[i][j] = content;
                    if (isReversed) j--;
                    else j++;
                }
                i++;
            }
        } else {
            int i = 0;
            for (List<Integer> nonBlankList : nonBlankMatrix) {
                if (isReversed)
                    Collections.reverse(nonBlankList);
                int j = isReversed ? boardRow-1 : 0;

                for (Integer content : nonBlankList) {
                    boardStructure[j][i] = content;
                    if (isReversed) j--;
                    else j++;
                }
                i++;
            }
        }
    }

    /**
     * Clone board for game over check
     * @param board
     * @return
     */
    private Board clone(Board board) {
        int boardRow = board.getNoOfRow();
        int boardColumn = board.getNoOfColumn();

        Board cloneBoard = new Board(boardRow, boardColumn, 0);
        int[][] originalBoard = board.getBoardStructure();
        int[][] cloneBoardStructure = cloneBoard.getBoardStructure();

        for (int i = 0; i < boardRow; i++) {
            for (int j = 0; j < boardColumn; j++) {
                cloneBoardStructure[i][j] = originalBoard[i][j];
            }
        }

        return cloneBoard;
    }
}
