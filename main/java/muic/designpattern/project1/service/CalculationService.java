package muic.designpattern.project1.service;

import muic.designpattern.project1.entity.Board;

import java.util.List;
import java.util.Map;

/**
 * Created by Trung on 9/12/2017.
 */
public interface CalculationService {

    /**
     * Calculate content for row when move right
     * @param board
     * @return
     */
    Board calculateBoardContentToRight(Board board);

    /**
     * Calculate content for row when move left
     * @param board
     * @return
     */
    Board calculateBoardContentToLeft(Board board);

    /**
     * Calculate content for row when move up
     * @param board
     * @return
     */
    Board calculateBoardContentToTop(Board board);

    /**
     * Calculate content for row when move down
     * @param board
     * @return
     */
    Board calculateBoardContentToBottom(Board board);

    /**
     * Find location of all blank nodes
     * @param board
     * @return
     */
    List<int[]> findAllBlankNodes(Board board);

    /**
     * Calculate the dimensions of the mainFrame
     * @return
     */
    int[] calculateMainFrameDimensions(int rows, int columns);

    /**
     * Calculate the dimensions of the GamePanel in the mainFrame
     * @return
     */
    int[] calculateGamePanelDimensions(int rows, int columns);

    /**
     * just return the size of the menu bar
     * @return
     */
    int[] calculateMenuBarDimensions(int columns);
}
