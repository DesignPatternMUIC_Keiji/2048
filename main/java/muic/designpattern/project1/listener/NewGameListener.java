package muic.designpattern.project1.listener;

import muic.designpattern.project1.controller.GamePanelController;
import muic.designpattern.project1.entity.Board;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class NewGameListener implements ActionListener{
    GamePanelController gamePanelController;

    public NewGameListener(GamePanelController gamePanelController) {
        this.gamePanelController = gamePanelController;
    }

    public void setGamePanelController(GamePanelController gamePanelController) {
        this.gamePanelController = gamePanelController;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        gamePanelController.getBoard().reset();
    }
}
