package muic.designpattern.project1.listener;

import muic.designpattern.project1.entity.Board;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashSet;

/**
 * Listener to detect for key event and update the board according to the direction
 */
public class GameMoveDirectionListener implements KeyListener{
    private Board board;
    private HashSet<Integer> validKeySet;
    private JLabel label = null;

    public GameMoveDirectionListener(Board board){
        this.board = board;
        validKeySet = new HashSet<Integer>();
        validKeySet.add(KeyEvent.VK_UP);
        validKeySet.add(KeyEvent.VK_DOWN);
        validKeySet.add(KeyEvent.VK_LEFT);
        validKeySet.add(KeyEvent.VK_RIGHT);
    }

    /**
     * for sanity checking to check if the listener is working
     * @param label
     */
    public void setLabel(JLabel label){
        this.label = label;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    /**
     * Press the key, if it's an arrow button, call the update on board and pass on the keycode
     * @param e
     */
    public void keyPressed(KeyEvent e){
//        System.out.println(e.getKeyCode());
        System.out.println(e.getKeyCode());
        if (validKeySet.contains(e.getKeyCode())){
            board.update(e.getKeyCode());
            if (label != null)
                this.label.setText(Integer.toString(e.getKeyCode()));
        }
    }

    public void keyReleased(KeyEvent e){
    }

    public void keyTyped(KeyEvent e){

    }

}

