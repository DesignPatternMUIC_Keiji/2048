package muic.designpattern.project1.gui.component;

import muic.designpattern.project1.service.CalculationService;
import muic.designpattern.project1.service.impl.CalculationServiceImpl;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class MainFrame extends JFrame{
    private int rows = 5;
    private int columns = 5;
    private final CalculationService calculationService = new CalculationServiceImpl();

    public MainFrame(){
        setUp();
    }

    public MainFrame(int rows, int columns){
        this.rows = rows;
        this.columns = columns;
        setUp();
    }

    public void setUp(){
        int[] mainFrameDimension = calculationService.calculateMainFrameDimensions(columns,rows);
        this.setSize(mainFrameDimension[0],mainFrameDimension[1]);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent windowEvent){
                System.exit(0);
            }
        });
    }


    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public void setColumns(int columns) {
        this.columns = columns;
    }
}
