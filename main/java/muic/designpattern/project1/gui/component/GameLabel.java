package muic.designpattern.project1.gui.component;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;

public class GameLabel extends JLabel {
    int value;
    final static HashMap<Integer,Color> COLOR_HASH_MAP = createColorHashMap();

    GameLabel(){
        this.setText(" ");
        this.setOpaque(true);
        this.setVerticalAlignment(SwingConstants.CENTER);
        this.setHorizontalAlignment(SwingConstants.CENTER);
    }

    //The only setter method that matters
    public void setValue(int value){
        this.value = value;
    }

    //Rerendering the boarder according to its current value
    public void updateLabel(){
        this.setBackground(COLOR_HASH_MAP.get(this.value));
        if (value == 0)
            this.setText(" ");
        else
            this.setText(Integer.toString(value));
    }

    static private HashMap<Integer,Color> createColorHashMap(){
        HashMap<Integer,Color> colorHashMap = new HashMap<>();
        colorHashMap.put(0, Color.darkGray);
        colorHashMap.put(2, Color.LIGHT_GRAY);
        colorHashMap.put(4, Color.GRAY);
        colorHashMap.put(8, Color.ORANGE);
        colorHashMap.put(16, Color.PINK);
        colorHashMap.put(32, Color.RED);
        colorHashMap.put(64, Color.YELLOW);
        colorHashMap.put(128, Color.CYAN);
        colorHashMap.put(256, Color.BLUE);
        colorHashMap.put(512, Color.GREEN);
        colorHashMap.put(1024, Color.GREEN);
        colorHashMap.put(2048, Color.MAGENTA);
        return colorHashMap;
    }
}
