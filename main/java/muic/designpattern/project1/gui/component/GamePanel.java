package muic.designpattern.project1.gui.component;

import muic.designpattern.project1.entity.Board;
import muic.designpattern.project1.service.CalculationService;
import muic.designpattern.project1.service.impl.CalculationServiceImpl;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;

public class GamePanel extends JPanel {
    CalculationService calculationService = new CalculationServiceImpl();
    GameLabel[][] gameLabels;
    int rows;
    int columns;

    public GamePanel(int rows, int columns){
        super();
        gameLabels = new GameLabel[rows][columns];
        this.rows  = rows;
        this.columns = columns;
        setUp();
    }

    /**
     * Create GameLabels and its 2d arrays that's keeping it, ensure that it's corresponded to how the boardStructure is
     */
    public void setUp(){
        gameLabels = new GameLabel[this.rows][this.columns];
        int[] dimensions = calculationService.calculateGamePanelDimensions(this.rows,this.columns);
        this.setSize(dimensions[0],dimensions[1]);
        this.setLayout(new GridLayout(this.rows,this.columns));
        for (int i = 0; i < this.rows; i ++){
            for (int j = 0; j < this.columns; j++){
                GameLabel gameLabel = new GameLabel();
                gameLabels[i][j] = gameLabel;
                this.add(gameLabel);
            }
        }
    }

    /**
     * given a board, change the state of each GameLabel
     * @param board
     */
    public void render(Board board){
        for (int i = 0; i < this.rows; i ++){
            for (int j = 0; j < this.columns; j++){
                gameLabels[i][j].setValue(board.getBoardStructure()[i][j]);
                gameLabels[i][j].updateLabel();
            }
        }

        /**
         * below is used to ensure that the actual grid have a proper view of the actual board
         */
        for (int i = 0; i < this.rows; i ++){
            for (int j = 0; j < this.columns; j++){
                gameLabels[i][j].doLayout();
                gameLabels[i][j].repaint();
            }
        }
        this.repaint();
        this.doLayout();

//        checkState(board);
    }

    /**
     * used to check if the actual game state is the same as the state in the GameLabel
     * @param board
     */
    public void checkState(Board board){
        System.out.println("Actual Game State");
        for (int i = 0; i < board.getBoardStructure().length; i++){
            System.out.println(Arrays.toString(board.getBoardStructure()[i]));
        }

        System.out.println("Displayed");
        for (int i = 0; i < board.getBoardStructure().length; i++){
            for (int j = 0; j < board.getBoardStructure().length; j++){
                String toPrint = gameLabels[i][j].getText().equals("") ? " " : gameLabels[i][j].getText();
                System.out.print( "|" +toPrint + "|");
            }
            System.out.println();
        }
    }
}
