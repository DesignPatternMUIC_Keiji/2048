package muic.designpattern.project1.gui.input.handler;

import muic.designpattern.project1.entity.Board;
import muic.designpattern.project1.service.BoardService;
import muic.designpattern.project1.service.impl.BoardServiceImpl;

import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Trung on 9/16/2017.
 */
public class InputHandler {

    private Board board;

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    private final BoardService boardService = new BoardServiceImpl();

    private final Map<Integer, Runnable> FUNCTIONS = new HashMap<Integer, Runnable>() {{
        put(KeyEvent.VK_DOWN, () -> boardService.moveDown(getBoard()));
        put(KeyEvent.VK_UP, () -> boardService.moveUp(getBoard()));
        put(KeyEvent.VK_RIGHT, () -> boardService.moveRight(getBoard()));
        put(KeyEvent.VK_LEFT, () -> boardService.moveLeft(getBoard()));
    }};

    public void handle(int keyEvent) {
        if (!FUNCTIONS.containsKey(keyEvent)) {
            throw new IllegalArgumentException("Don't tryna do some specky shit you hooligan: " + keyEvent);
        }
        FUNCTIONS.get(keyEvent).run();
    }
}
