package muic.designpattern.project1.game.observer;

import muic.designpattern.project1.entity.Board;
import muic.designpattern.project1.gui.component.GamePanel;

import javax.swing.*;
import java.util.Observable;
import java.util.Observer;

/**
 *  The actual view
 *  Contain gamePanel, gameLabel
 */
public class BoardObserver implements Observer{
    private Board board = null;
    private GamePanel gamePanel = null;

    public BoardObserver(GamePanel gamePanel, Board board){
        setGamePanel(gamePanel);
        setBoard(board);
    }

    public BoardObserver(int rows, int columns){
        gamePanel = new GamePanel(rows,columns);
        gamePanel.setUp();
    }

    private void setGamePanel(GamePanel gamePanel){
        this.gamePanel = gamePanel;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    /**
     * the implemented method for the Observer interface
     * @param o
     * @param arg
     */
    public void update(Observable o, Object arg) {
        board = (Board) o;
        if (board.isGameOver()){
            JOptionPane.showMessageDialog(null, "Game Over!");
            board.reset();
        }
        gamePanel.render(board);
    }

    public Board getBoard() {
        return board;
    }

    public GamePanel getGamePanel() {
        return gamePanel;
    }

}
