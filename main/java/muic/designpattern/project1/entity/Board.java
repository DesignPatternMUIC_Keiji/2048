package muic.designpattern.project1.entity;


import muic.designpattern.project1.gui.input.handler.InputHandler;
import muic.designpattern.project1.service.BoardService;
import muic.designpattern.project1.service.impl.BoardServiceImpl;

import java.util.Observable;

/**
 * Created by Trung on 9/12/2017.
 */
public class Board extends Observable{

    private int row;
    private int column;
    private int initSpawn;
    private int[][] boardStructure;
    private boolean isGameOver = false;

    private final BoardService boardService = new BoardServiceImpl();

    public Board(int row, int column, int initSpawn) {
        this.row = row;
        this.column = column;
        this.initSpawn = initSpawn;
        this.boardStructure = new int[row][column];
        boardService.setBoard(this, this.initSpawn);
    }

    public int getNoOfRow() {
        return row;
    }

    public int getNoOfColumn() {
        return column;
    }

    public int[][] getBoardStructure() {
        return boardStructure;
    }

    public boolean isGameOver() {
        return isGameOver;
    }

    public void setGameOver(boolean gameOver) {
        isGameOver = gameOver;
    }

    /**
     * Make this function change something in the board
     * Notify observer and rerender the gamePanel
     * @param key
     */
    public void update(int key){
        InputHandler inputHandler = new InputHandler();

        // Do user input
        inputHandler.setBoard(this);
        inputHandler.handle(key);

        // Update Game Over state
        boolean isGameOver = boardService.checkGameOverStatus(this);
        this.setGameOver(isGameOver);

        // Attempt to spawn after each move
        boardService.spawnBoardContent(this, 1);
        callChange();
    }

    /**
     * Reset board after game over
     */
    public void reset(){
        boardService.setBoard(this, this.initSpawn);
        callChange();
    }

    /**
     * Notify observer on change
     */
    public void callChange(){
        setChanged();
        notifyObservers();
    }
}
