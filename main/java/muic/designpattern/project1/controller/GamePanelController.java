package muic.designpattern.project1.controller;

import muic.designpattern.project1.entity.Board;
import muic.designpattern.project1.game.observer.BoardObserver;
import muic.designpattern.project1.gui.component.GamePanel;
import muic.designpattern.project1.listener.GameMoveDirectionListener;

public class GamePanelController {

    Board board;
    GamePanel gamePanel;


    public GamePanelController(int rows, int columns){
        this.gamePanel = new GamePanel(rows,columns);
        this.board = new Board(rows,columns,4);

        BoardObserver boardObserver = new BoardObserver(this.gamePanel,board);

        this.board.addObserver(boardObserver);
        this.board.callChange();

    }

    public GamePanel getGamePanel() {
        return gamePanel;
    }

    public Board getBoard() {
        return board;
    }
}
