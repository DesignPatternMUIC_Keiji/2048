package muic.designpattern.project1.controller;

import muic.designpattern.project1.gui.component.MainFrame;
import muic.designpattern.project1.listener.CustomizeGameListener;
import muic.designpattern.project1.listener.GameMoveDirectionListener;
import muic.designpattern.project1.listener.NewGameListener;
import muic.designpattern.project1.service.CalculationService;
import muic.designpattern.project1.service.impl.CalculationServiceImpl;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;

public class GameController {

    MainFrame mainFrame = null;
    GamePanelController gamePanelController = null;
    GameMoveDirectionListener gameMoveDirectionListener = null;
    final CalculationService calculationService = new CalculationServiceImpl();
    HashMap<String,JMenuItem> menuItemHashMap = new HashMap<>();

    /**
     * Game controller is basically the mothership of controllers
     * it contains controller such as GamePanelController which controls the actual game grid and its rendering
     */
    public GameController(){
        mainFrame = new MainFrame();
        mainFrame.setTitle("2048");
        JMenuBar menubar = new JMenuBar();
        JMenu menu = new JMenu("Options");
        JMenuItem newGameItem = new JMenuItem("New Game");
        JMenuItem customizeGameItem = new JMenuItem("Customize Dimensions");

        menuItemHashMap.put("newgame",newGameItem);
        menuItemHashMap.put("customize",customizeGameItem);
        menu.add(newGameItem);
        menu.add(customizeGameItem);
        menubar.add(menu);
        mainFrame.setJMenuBar(menubar);

        setUpGame(mainFrame.getRows(),mainFrame.getColumns());
        newGameItem.addActionListener(new NewGameListener(this.gamePanelController));
        customizeGameItem.addActionListener(new CustomizeGameListener(this));

        mainFrame.setVisible(true);
    }

    /**
     * call this function whenever there's a change to the size of the game.
     * This is usually used with an Observable or a listener (listener usually)
     * @param rows
     * @param columns
     */
    public void setUpGame(int rows, int columns){
        int[] mainFrameDimensions = calculationService.calculateMainFrameDimensions(rows,columns);

        //Size setting of the frame
        this.mainFrame.setRows(rows);
        this.mainFrame.setColumns(columns);
        this.mainFrame.setSize(mainFrameDimensions[0],mainFrameDimensions[1]);
        this.mainFrame.getJMenuBar().setSize(calculationService.calculateMenuBarDimensions(mainFrame.getColumns())[0],
                calculationService.calculateMenuBarDimensions(mainFrame.getColumns())[1]);

        //Adding GamePanel into the mainframe
        if (gamePanelController != null){
            this.mainFrame.remove(this.gamePanelController.getGamePanel());
            this.gamePanelController = null;
        }
        this.gamePanelController = new GamePanelController(rows,columns);
        this.mainFrame.add(this.gamePanelController.getGamePanel());

        //Adding and changing the board and gameMoveDirectionListener
        if (gameMoveDirectionListener == null){
            this.gameMoveDirectionListener = new GameMoveDirectionListener(this.gamePanelController.getBoard());
            this.mainFrame.addKeyListener(this.gameMoveDirectionListener);
        }
        else
            this.gameMoveDirectionListener.setBoard(this.gamePanelController.getBoard());

        //This line is abit confusing, we know that newgame button has only one action listener, but the gamepanelcontroller
        //that action listener has is going to be outdated, so we set the new gamepanelcontroller into it
        //we also know that the newgame button only has 1 actionlistener
        if (menuItemHashMap.get("newgame").getActionListeners().length != 0){
            ((NewGameListener) menuItemHashMap.get("newgame").getActionListeners()[0]).setGamePanelController(gamePanelController);
        }
    }

    public MainFrame getMainFrame() {
        return mainFrame;
    }
}
